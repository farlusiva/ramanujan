#!/usr/bin/env python3
# formatting.py: Functions for formatting digits and progress

# A status bar to be used in logging
def status_bar(k, rounds):

	dbg_funcs.debug(f"Working. { ( str((k/rounds) * 100)[:2]) if (k/rounds)* 100 > 10 else ( str((k/rounds) * 100)[:1]) }% Finished. ({k} rounds out of {rounds})")


# Correct the length of the input. Used in format_layer1.
# inp: The input, prec: getcontext().prec.
def correct_length(inp, prec):

	len_after_dot1 = inp.find(".")
	before = inp[:len_after_dot1] # Everything that len_after_dot2 is not.
	len_after_dot2 = inp[len_after_dot1+1:]
	len_after_dot = len(len_after_dot2)

	dbg_dfdm.debug(f"Before the \".\": \"{before}\"")
	dbg_dfdm.debug(f"Dot position in inp: {len_after_dot1}")
	dbg_dfdm.debug(f"Raw digits: {len_after_dot2[:20]}...")
	dbg_dfdm.debug(f"Length of raw digits: {len_after_dot}")
	dbg_dfdm.debug(f"Expected length of raw digits: {prec - len(before)}")
	dbg_dfdm.debug(f"Desired prec (i.e. getcontext().prec): {prec}")

	# Zero-spam test, will catch anything of not sufficient precision
	# (for example, hillclimb 1/8 = x), and pad it with zeroes until it is of
	# sufficient length.
	if prec - len(before) > len_after_dot + len(before):

		# There is a comma and to_return is shorter than the precision set, so for example with hillclimbv1 maybe x = 1/8 = 0.125
		dbg_dfdm.debug(f"Performing zero-spam because {prec - len(before)} > {len_after_dot + len(before)}")

		# "Zero spamming" formatting method
		# Adds zeroes until it is of length
		# which the format_constant can handle
		# format_constant can handle of any length
		# longer than line_length, but it will just
		# ignore the last digits, which would be
		# annoying if you want 1k pi (at defualt settings) but get 950.

		inp = inp + "0" * (length-1 - len_after_dot)

		dbg_dfdm.debug("After zero-spam formatting:")
		dbg_dfdm.debug(f"Added {length-1 - len_after_dot} zeroes")
		dbg_dfdm.debug(f"New length of raw digits: {len(inp[inp.find('.')+1:])}")
		dbg_dfdm.debug(f"Total length: {len(inp)}")

		# Nothing is returned here so other tests can do thier thing too,
		# like the rounding stuff

	# Truncate extra digits so the amount of digits will match that of the expected value
	# If this code wouldn't be here, then format_digits would probably format another
	# row of digits
	elif prec - len(before) < len_after_dot:
		# Elif because we shouldn't ever need to truncate digits if zero-spamming
		# works as it should and zero-spams to the correct length

		dbg_dfdm.debug(f"Performing extra truncation because {prec - len(before)} < {len_after_dot}")


		# Original was -(len_after_dot - (prec - len(before)))
		inp = inp[ : prec - len(before) - len_after_dot]

		dbg_dfdm.debug("After extra truncation:")
		dbg_dfdm.debug(f"New length of raw digits: {len(inp[inp.find('.')+1:])}")
		dbg_dfdm.debug(f"Total length: {len(inp)}")


	return inp



# Will round the input if it goes like this:
# x.999999... or x.000000...
# Used in format_layer1
def rounding(inp):

	# 9er / 0er count tests
	# Will round down or up if to_format is something like
	# 1.99999999... or 2.000000000...


	# This will run in most cases, more specifically when full precision is used
	# for calculation. That means it will run on Pi, but not hillclimb (x = 1/8)

	## We need to add len(before) BECAUSE FOR SOME STUPID REASON the decimal Library
	## precision is NOT the floating point precision.
	## For example, the root of 9999999999999999999 is an integer! (hint: it's not)
	## They include the integer part in the precision!!!
	## This wouldn't have been a problem if ramanujan only calculated constants
	## with one digit before the decimal point (less than 10), but here we have
	## roots and (hills to climb), so the code must account for that.


	# We're only testing on the digits visible to the user, so that we won't get
	# into an awkward situation of "4.9999" but without the final "...6" or something
	# to clearly state that this is *not* an integer

	len_after_dot1 = inp.find(".")
	before = inp[:len_after_dot1] # Everything that len_after_dot is not.
	len_after_dot = inp[len_after_dot1 + 1:]


	# Make it easier to understand how many 9ers or 0ers are needed for the
	# special cases

	if disable_rounding:
		dbg_dfdm.debug("Rounding is disabled, returning inp")

	else:

		dbg_dfdm.debug(f"Amount of 9ers or 0ers needed for rounding: {len(len_after_dot)}")
		dbg_dfdm.debug(f"9er count: {len_after_dot.count('9')}")
		dbg_dfdm.debug(f"0er count: {len_after_dot.count('0')}")


		# Special cases for ?.999999 and ?.00000
		# They have to be done after the extra digits removal so they
		# will do things like 1.99 when with extra digits was 1.999998

		if len_after_dot.count("9") >= len(len_after_dot):

			dbg_dfdm.debug("There were lots of 9's. Returning \"before + 1\".")
			return str(int(before) + 1)

		if len_after_dot.count("0") >= len(len_after_dot):

			dbg_dfdm.debug("There were lots of 0's. Returning \"before\".")
			return str(int(before))


		dbg_dfdm.debug("If you are reading this, rounding is enabled but there were not enough nines or zeroes to do the rounding")


	# No rounding needs to be done
	return inp



def format_layer1(to_format):
	# The job of layer1 is to remove unneeded digits at the end of the digits
	# to calc, and also to add zeroes if nessecary.

	# Make sure the input is a string
	to_format = str(to_format)


	dbg_dfdm.debug("Formatting digits in layer1. aka getting rid of extra_digits")

	# Only print the first and last few digits if the digits being formatted
	# are too long
	if len(to_format) > 20:
		dbg_dfdm.debug(f"Formatting: {to_format[:10]} ... {to_format[-10:]}")
	else:
		dbg_dfdm.debug(f"Formatting: {to_format}")


	if "." not in to_format:
		# There is no decimal point, so there will be no formatting done here.

		dbg_dfdm.debug("There is no \".\" in the digits, returning all the digits")
		return to_format

	dbg_dfdm.debug("There is a \".\" in the digits.")

	# 'Correct' the length, by truncating or adding zeroes.
	# Put new formatting code above here.
	to_format = correct_length(to_format, getcontext().prec)

	to_return = to_format[ : -extra_digits - 1]
	dbg_dfdm.debug(f"extra_digits removed, len_after_dot now effectively {len(to_return[to_return.find('.'):])}")

	# Round to_return (returns to_return if there is no rounding)
	to_return = rounding(to_return)


	# Even if rounding is off, we did previously set to_return,
	# so we can just return it.
	return to_return



def format_digits(the_input):
	# Formats digits so they are pleasing to look at
	# (well, more than it's unformatted counterpart)

	# Ensure that the input is a string
	the_input = str(the_input)

	# No formatting needs to be done
	if "." not in the_input:
		return the_input

	# Constant without the "n."
	temp_the_input = the_input[the_input.find(".")+1:]

	# The "n.", plus a newline
	formatted_string = the_input[:the_input.find(".")+1] + "\n"

	for a in range(0 , len(temp_the_input) - line_length, line_length):
	# The formatting code feels like spaghetti after actually being finished with it

		# A single 'line' of digits, as shown when format_digits is on
		temp = temp_the_input[a : a + line_length]

		# Do no wrapping when line_spacing is 0
		if line_spacing > 0:
			temp = " ".join(wrap(temp, line_spacing)) # Add periodic spaces

		formatted_string += f"{temp} | {a + line_length}\n"


	return formatted_string

def format_const_to_calc():
	# Formats the const_to_calc to be user-friendly you could say.
	# For example, with e and Pi there is nothing changed, but with sqrt(n)
	# and nth root of x are changed to for example sqrt(163) and 3rd root of 89.

	cur_family = fetch_family_name(const_to_calc)

	if cur_family == "sqrt":
		return f"Sqrt({sqrt_n_calc})"

	elif cur_family == "nthrootofx":


		if   nth_root_of_x_n == 1: temp = "st"
		elif nth_root_of_x_n == 2: temp = "nd"
		elif nth_root_of_x_n == 3: temp = "rd"
		else:                      temp = "th"

		return f"{nth_root_of_x_n}{temp} root of {nth_root_of_x_x}"

	elif cur_family == "hillclimb":
		return f"Hillclimb ({hillclimb_expression1} = {hillclimb_expression2})"


	# Most of the time, a prettified family name is enough.
	return fetch_pretty_family_name(fetch_family_name(const_to_calc))

def turn_into_decimal(maybe_decimal):

	# Turn input string into a decimal as error prevention
	# Will turn "897asudhiaUBIASHDAISUHD123KJBNASD" into Decimal(897123)

	# I could also make it so ramanujan refuses to accept input if it is
	# not a float / int (depending on usecase), but i came up with doing
	# this first.

	result, ints, can_add_dot = "", "0123456789", True

	for a in str(maybe_decimal):

		if a == "." and can_add_dot:
			# At this point, whenever this is ran a is always a "."
			result += a
			can_add_dot = False

		if a in ints:
			result += a


	# Will also work when the length of result is 0
	if result == "." * len(result):
		return None

	return D(result)


# Use turn_into_decimal to get a positive nonzero number from the user.
# 'string' is the string to print, i.e. what the user is asked for.
def get_pos_num(string):

	while True:
		inp = turn_into_decimal(input(string))
		try:
			if inp > 0:
				return inp

		except TypeError:
			continue

