#!/usr/bin/env python3
# funcs.py: The functions to calculate constants


# Print function for debug mode. Will only print when debug is enabled.
# NOTE: Usage of this for debugging purposes is deprecated.
def dprint(inp):

	if debug:
		print(inp)


# Optimized formula from wikipedia of Chudnovsky formula

# Around 10 seconds for 10k pi with default the rest
# For refrence, the chudnovsky used before that is around 15,
# and the old ramanujan is roughly 70.

def pi_chudnovsky():

	rounds = int(length / 14) + 1

	dbg_funcs.debug(f"Calculting pi with a total precision of {getcontext().prec}, calculating for {rounds} rounds.")
	dbg_funcs.debug(f"series_extra_rounds = {series_extra_rounds}")
	dbg_funcs.debug("Calculating initial variables")


	C = D(426880 * D(10005).sqrt())

	M, K, L, X = 1, 6, 13591409, 1

	the_sum = D(13591409) # First round with current MKLX pre-calculated


	dbg_funcs.debug("Infinite series started.")

	for k in range(1, rounds):
		# k goes from 1 to rounds instead of starting at 0
		# so there is no need for (k+1)**3 in the M calc

		if k % 10 == 0 and k > 0:
			status_bar(k, rounds)


		M *= D(K**3 - 16*K) / k**3
		K += 12
		L += 545140134
		X *= -(640320**3)

		the_sum += ( D(M) * D(L) ) / D(X)


	dbg_funcs.debug("Doing final multiplication")

	pi = D(C) * ( D(the_sum) ** D(-1))


	return format_layer1(str(pi))

# gmpy2-ified version of pi_chudnovsky
# it seems that gmpy is much quicker than the python decimal library
def pi_chudnovsky_gmpy():

	rounds = int(length / 14) + 1

	dbg_funcs.debug(f"Calculting pi with a total precision of {getcontext().prec}, calculating for {rounds} rounds.")
	dbg_funcs.debug(f"series_extra_rounds = {series_extra_rounds}")
	dbg_funcs.debug("Calculating initial variables")


	C = mpfr(426880)* gmpy2.sqrt(mpfr(10005))

	M, K, L, X = mpfr(1), mpfr(6), mpfr(13591409), mpfr(1)

	the_sum = mpfr("13591409") # First round with current MKLX pre-calculated


	dbg_funcs.debug("Infinite series started.")

	for k in range(1, rounds):
		# k goes from 1 to rounds instead of starting at 0
		# so there is no need for (k+1)**3 in the M calc

		if k % 10 == 0 and k > 0:
			status_bar(k, rounds)


		M *= mpfr(K**3 - 16*K) / k**3
		K += 12
		L += 545140134
		X *= -(640320**3)

		the_sum += ( M * L ) / X


	dbg_funcs.debug("Doing final multiplication")

	pi = C * (1 / the_sum)


	return format_layer1(str(pi))



# http://www.craig-wood.com/nick/images/math/40f2e34873e611fb04c7b93ed846b724.png
# (The middle one)
def pi_chudnovsky_alt(): # Variant of the chudnovsky algorithm

	rounds = int(length / 14) + series_extra_rounds # Ten extra rounds should be able to calculate all of the extra_digits accurately

	the_sum = 0

	dbg_funcs.debug(f"Calculting pi with a total precision of {getcontext().prec}, calculating for {rounds} rounds.")
	dbg_funcs.debug(f"series_extra_rounds = {series_extra_rounds}")


	for k in range(0, rounds):

		if k % 10 == 0 and k > 0:
			status_bar(k, rounds)


		upper_part = D( (D((-1)**k)) * fact(6*k) * (545140134*k + 13591409))
		lower_part = D( fact(3*k) * ( fact(k)**3 ) * ( D(640320) ** D(3*k)))

		the_sum += upper_part / lower_part

	dbg_funcs.debug("Finished with the infinite series. Calculating the rest.")

	#the_sum *= D(1) / ( D(42688) * D(10005).sqrt() )
	# Using this (^) will return ten times less than pi. Am too scared to do anything.
	the_sum *= D(12) / ( D(640320) * D(640320).sqrt() )


	pi = D(1) / D(the_sum)


	return format_layer1(str(pi))



def pi_ramanujan():
	# Basically a copy of the picalc code in backup # 7

	rounds = int(length / 7) + series_extra_rounds

	dbg_funcs.debug(f"Calculting pi with a total precision of {getcontext().prec}, calculating for {rounds} rounds.")
	dbg_funcs.debug(f"series_extra_rounds = {series_extra_rounds}")


	# Using sqrt(8) here is the same as 2*sqrt(2)
	first_part = D(8).sqrt()/D(9801)

	the_sum = D(0)

	for k in range(0, rounds):

		if k % 10 == 0 and k > 0:
			status_bar(k, rounds)


		upper = fact(4*k) * (1103 + 26390*k)
		lower = (fact(k)**4) * (396**(4*k))

		the_sum += D(upper) / D(lower)


	one_over_pi = D(first_part) * D(the_sum)

	pi = D(1) / D(one_over_pi)

	return format_layer1(str(pi))


def golden_common():

	dbg_funcs.debug("Calculating sqrt(5)...")

	sqrt_5 = D(5).sqrt()

	dbg_funcs.debug("Finished with sqrt(5), now going over to (1 + sqrt5) / 2.")


	final = ( D(1) + D(sqrt_5) ) / D(2)

	return format_layer1(str(final))


# proof:
#  know: -1/phi = (1-sqrt(5))/2
#  (1-sqrt(5))/2 = -1 / phi
#  2 / (1-sqrt(5)) = phi / -1
#  2 / (1-sqrt(5)) = -phi
#  -2 / (1-sqrt(5)) = phi
def golden_alt():

	dbg_funcs.debug("Caluclating sqrt(5)...")

	sqrt_5 = D(5).sqrt()

	dbg_funcs.debug("Finished with sqrt(5), now going over to -2 / (1 - sqrt(5))")

	final = D(-2) / ( D(1) - D(sqrt_5) )

	return format_layer1(str(final))



def nth_root_of_x(*args):

	if len(args) >= 2:
		n, x = args[0], args[1]

	else:
		n, x = nth_root_of_x_n, nth_root_of_x_x


	dbg_funcs.debug("Calculating nth root of x")

	# Same optimization as sqrt(n) uses,
	# am unsure if there are more that are
	# Deicmal optimized.
	final = D(x) ** ( D(1) / D(n))

	return format_layer1(str(final))


def nth_root_of_x_log(*args):

	if len(args) >= 2:
		n, x = args[0], args[1]

	else:
		n, x = nth_root_of_x_n, nth_root_of_x_x

	# Uses the same identity as sqrt_log

	dbg_funcs.debug("Calculating nth root of x")


	final = D( 10 ** ( D(x).log10() / D(n)))

	return format_layer1(str(final))



def e_general():

	dbg_funcs.debug("Calculating e")

	final = D(1).exp()
	# the decimal libary's calculation code is fast as heck.
	# here i'm just calculating e**1

	return format_layer1(str(final))


def e_series():

	the_sum = D(1)
	to_add = 1

	n = 1

	basically_zero = 10 ** ( - getcontext().prec + 1)

	while True:

		to_add = to_add / D(n)

		# Exit conditions
		if to_add < basically_zero or the_sum == the_sum + to_add:
			break


		the_sum += to_add



		n += 1

	return format_layer1(str(the_sum))



def sqrt_general(*args):

	# Basically, there are two ways to run sqrt_general with args set up like
	# this.
	# 1)
	# Running sqrt_general(). sqrt_general will use the global variable sqrt_n_calc
	# as what it'll root.
	# 2)
	# Giving sqrt_general input. The first item of that will be what it'll root.
	# Ex: sqrt_general(2)

	if len(args) >= 1:
		tocalc = args[0]
	else:
		tocalc = sqrt_n_calc


	dbg_funcs.debug(f"Calculating sqrt({tocalc})")

	final = D(tocalc).sqrt()
	return format_layer1(str(final))



def sqrt_pow_half(*args):
	# Alternative to sqrt_general(n), where
	# the number is instead being raised to one half.

	if len(args) >= 1:
		tocalc = args[0]
	else:
		tocalc = sqrt_n_calc

	dbg_funcs.debug(f"Calculating sqrt({tocalc})")

	final = D(tocalc) ** D(1/2)

	return format_layer1(str(final))

def sqrt_log(*args):
	# Uses the identity sqrt(x) = 10 ** ( log10( x ) / 2 )

	if len(args) >= 1:
		tocalc = args[0]
	else:
		tocalc = sqrt_n_calc

	dbg_funcs.debug(f"Calculating sqrt({tocalc})")

	part1 = D(tocalc).log10() / D(2)

	final = D(10) ** D(part1)


	return format_layer1(str(final))





# Note, this is horribly optimized as the decimal libary sqrt is fast as heck
# compared to using the normal nth root of x method, which is used here. Twice.


# 1k digits took about 0.4 seconds, while
# 10k digits took about a minute on my pc

# so 10 times the digits, 150 times the calculation time.
# i believe the part that takes most time is the third root part
# which could probably be optimized





# Source: http://planetmath.org/plasticconstant
# Archived version: https://web.archive.org/web/20160911055548/http://planetmath.org/plasticconstant
# (This is a slightly modified version of the first one)

def plastic_general():

	dbg_funcs.debug("Calculating sqrt(69)/18")


	# The part that is unchanged, (1/6) * sqrt(23/3) is basically sqrt(69)/18
	unc = D(69).sqrt() / D(18)
	one_third = D(1) / D(3)



	dbg_funcs.debug("Calculating first part: cbrt(0.5 + sqrt(69)/18)")

	first_part = D( D(0.5) + unc) ** one_third


	dbg_funcs.debug("Calculating second part: cbrt( 0.5 - sqrt(69)/18)")

	second_part = D( D(0.5) - unc) ** one_third

	dbg_funcs.debug("Summing the two parts...")

	final = D(first_part) + D(second_part)

	return format_layer1(str(final))



# Used to be main, but a slightly faster one was found
# New one has been cross-checked with hillclimb and this one,
# and the tests concluded with that it was slightly faster
# and returns the same result.
def plastic_alternative1():

	dbg_funcs.debug("Calculating 12*sqrt(69).")

	twelve_sqrt_sixtynine = D(12) * D(69).sqrt()
	onethird = D(1) / D(3)

	dbg_funcs.debug("Calculating first part: cbrt(108 + 12*sqrt(69)).")

	upper_1 = (108 + twelve_sqrt_sixtynine) ** onethird

	dbg_funcs.debug("Calculating second part: cbrt(108 + 12*sqrt(69)).")

	upper_2 = (108 - twelve_sqrt_sixtynine) ** onethird

	upper = upper_1 + upper_2

	dbg_funcs.debug("Doing the final division")

	final = D(upper) / D(6)

	return format_layer1(str(final))


# Algorithm obtained from http://mathworld.wolfram.com/PlasticConstant.html

def plastic_alternative2():

	dbg_funcs.debug("Calculating sqrt(69)")

	sqrt_69 = D(69).sqrt()
	one_third = D(1) / D(3)



	dbg_funcs.debug("Calculating cbrt( 9 - sqrt(69) )")

	part_one = D(9 - sqrt_69) ** one_third

	dbg_funcs.debug("Calculating cbrt( 9 + sqrt(69) )")

	part_two = D(9 + sqrt_69) ** one_third


	dbg_funcs.debug("Calculating lower part")

	lower = D(2) ** one_third * ( D(3) ** ( one_third * 2))


	dbg_funcs.debug("Doing the final division.")

	final = D(part_one + part_two) / D(lower)

	return format_layer1(str(final))




# This is even slower than the calc code for the plastic constant -_-

def zeta3_wedeniwski(): # Zeta3, formula by Sebastian Wedeniwski

	rounds = int(length/5) + series_extra_rounds


	dbg_funcs.debug(f"Calculting zeta3 with a total precision of {getcontext().prec}, calculating for {rounds} rounds.")
	dbg_funcs.debug(f"series_extra_rounds = {series_extra_rounds}")


	def P(k):
		a = 126392*k**5
		b = 412708*k**4
		c = 531578*k**3
		d = 336367*k**2
		e = 104000*k
		f = 12463

		return a + b + c + d + e + f


	the_sum = 0

	for k in range(0, rounds):


		if k % 10 == 0 and k > 0: # If you want comments then check in the pi_chudnovsky code
			status_bar(k, rounds)


		upper = D((fact(2*k+1) * fact(2*k) * fact(k))**3)
		lower = D(24 * fact(3*k+2) * fact(4*k+3)**3)



		the_sum += D(-1)**k * ( D(upper) / D(lower) ) * P(k)


	return format_layer1(str(the_sum))


def zeta3_mohamud():
	# Formula found by Found by Mohamud Mohammed in 2005

	rounds = int(length / 3.9) + series_extra_rounds

	the_sum = 0
	first_part = D(1/2)


	dbg_funcs.debug("Calculting zeta3 with a total precision of {getcontext().prec}, calculating for {rounds} rounds.")
	dbg_funcs.debug(f"series_extra_rounds = {series_extra_rounds}")


	def P(k):
		a = 40885*k**5
		b = 124346*k**4
		c = 150160*k**3
		d = 89888*k**2
		e = 26629*k
		f = 3116

		return D(a + b + c + d + e + f )



	for k in range(0, rounds):

		if k % 10 == 0 and k > 0:
			status_bar(k, rounds)



		upper = D( (-1) ** k * fact(2*k) ** 3 * fact(k + 1)**6)
		lower = D( (k + 1) ** 2 * fact(3*k + 3) ** 4 )

		the_sum += (D(upper) / D(lower)) * P(k)


	final = D(first_part) * D(the_sum)

	return format_layer1(str(final))





def hillclimbv1(*args):
	# "x" is the answer to the
	# given problem, so that it would be for example typed as
	# x+1 = x**3, or something.

	global a_or_b

	if len(args) >= 2:
		expression1, expression2 = args[0], args[1]

	else:
		expression1, expression2 = hillclimb_expression1, hillclimb_expression2





	def the_calc_code(expression1, expression2, a_or_b, time_limit):
	# a_or_b is b if the answer is less than 0.1, and it is a if less than 0.1.
	# Could also be defined as if the answer to a, or b is the bigger number
	# however if defined like that then the code will break if the answer is less than 0.1.

		x = D("0.1")
		smallest = D("0.1")


		start_time = time()

		zero = 10** ( - getcontext().prec + 2)


		while True:

			if time_limit != 0 and time() > time_limit:
				return "Error"


			# Used to be some code here
			# to remove the last digit if
			# a or b is a float,
			# removing because it messed up
			# the format_layer1 code,
			# the latest version that had the old
			# version of this is backup #20.


			## Took back the code here that is in backup #20
			## because the format_layer1 code is no longer broken.
			## This is probably because of the zero-spamming method.

			### Came back to check on this, backupversion #21 does
			### not support the zero-spamming method. That is probably why.


			exp1_eval = str(eval(expression1))

			if "." in exp1_eval:
				exp1_eval = D(exp1_eval[:-1])

			else:
				exp1_eval = D(exp1_eval)


			exp2_eval = str(eval(expression2))

			if "." in exp2_eval:
				exp2_eval = D(exp2_eval[:-1])

			else:
				exp2_eval = D(exp2_eval)




			if a_or_b == "b":
				c = exp2_eval - exp1_eval

			elif a_or_b == "a":
				c = exp1_eval - exp2_eval

			dbg_funcs.debug("-"*30)
			dbg_funcs.debug(f"exp1_eval: {exp1_eval}")
			dbg_funcs.debug(f"exp2_eval: {exp2_eval}")
			dbg_funcs.debug(f"c (diff between exp1 and exp2) = {c}")
			dbg_funcs.debug(f"smallest (to_add) {smallest}")



			if c < 0:
				x = x - smallest

				x = D(f"{x}1")

				smallest /= jump_divider

				dbg_funcs.debug("-"*30)
				dbg_funcs.debug(f"Lowering smallest. Now {smallest}")
				dbg_funcs.debug(f"x now {x}")
				dbg_funcs.debug(f"smallest now {smallest}")

			elif c > 0:
				x += smallest


			elif exp1_eval == exp2_eval:
				final = str(x)

				dbg_funcs.debug(f"We have a winner! {x}")

				return final



	dbg_funcs.debug(f"a_or_b = {a_or_b}")

	if a_or_b.lower() == "auto":

		time_threshold = 2 # Amount of extra calculation seconds given
		getcontext().prec = 10 + extra_digits # Determine the value of a_or_b by trying
		# both cases and seeing which actually gives an answer.
		a_or_b = "b"
		dbg_funcs.debug("A or b testing started.")


		b_time_start = time()
		temp_b = the_calc_code(expression1, expression2, "b", 10)
		b_time_stop = time()



		a_time_start = time()
		temp_a = the_calc_code(expression1, expression2, "a", b_time_stop - b_time_start + time_threshold)

		a_time_stop = time()

		if temp_a == "Error":
			a_or_b = "b"

		elif temp_b == "Error": # might be a, rechecking with b with a time + 2

			b_time_start = time()

			temp_b = the_calc_code(expression1, expression2, "b", b_time_stop  - b_time_start + time_threshold)

			b_time_stop = time()

			if temp_b == "Error": # b failed test with 10 seconds AND with a time + 2, it is a time.

				a_or_b = "a"

			else:
				a_or_b = "b"


		getcontext().prec = length

	dbg_funcs.debug(f"Started actual calculation. a_or_b = {a_or_b}")


	final = the_calc_code(expression1, expression2, a_or_b, 0)

	return format_layer1(str(final))
	# Not having the standard extra "-1" because it makes the length 1 less
	# than expected to be returned.





# Performance numbers: (Calculated in the initial version)
# 500 digits omega constant:
# v1: 14.6
# v2: 13.1



def hillclimbv2(*args):

	x = D( -big_number - 1)
	jump = D(big_number)

	# The reason for the "-1" in the initial x calculation is because it will then avoid
	# trying to calculate with x = 0, which could break some thing (like division by 0)
	# unless the answer is zero.

	if len(args) >= 2:
		expression1, expression2 = args[0], args[1]

	else:
		expression1, expression2 = hillclimb_expression1, hillclimb_expression2


	start_time = time()

	answer = "Error: Managed to be finished with calculating, yet not change the answer"
	# Error handling

	if a_or_b == "a":
		old_val = D(str(eval(expression1))) - D(str(eval(expression2)))

	elif a_or_b == "b":
		old_val = D(str(eval(expression2))) - D(str(eval(expression1)))

	else:
		old_val = D(str(eval(expression1))) - D(str(eval(expression2)))

	# val is for value, stat is for status.
	# old is where the previous "jump" was, new is where it currently is.
	if old_val > 0:
		old_stat = "Positive"

	elif old_val < 0:
		old_stat = "Negative"

	elif old_val == 0:
		answer = x

		dbg_funcs.debug("Lucky! Got the answer first try in hillclimbv2")
		dbg_funcs.debug(f"The winner is {answer}")

		return format_layer1(str(answer))

	dbg_funcs.debug("Initial setup:")
	dbg_funcs.debug(f"expression1: {expression1}")
	dbg_funcs.debug(f"expression2: {expression2}")
	dbg_funcs.debug(f"x = {x}")
	dbg_funcs.debug(f"jump = {jump}")
	dbg_funcs.debug(f"a_or_b = {a_or_b}")
	dbg_funcs.debug(f"old_val = {old_val}")
	dbg_funcs.debug(f"old_stat = {old_stat}")




	while True:

		x += jump


		# Use a variable for the eval of the expressions, so that they don't
		# need to be eval'd again in the debug messages.
		exp1_eval = D(str(eval(expression1)))
		exp2_eval = D(str(eval(expression2)))


		# Originally, hcv2 was designed with a hardcoded value of a_or_b,
		# because it was irrelevant. Nowadays it supports it, as to follow
		# hcv1, wherein it does actually matter.
		if a_or_b == "a":
			new_val = exp1_eval - exp2_eval

		elif a_or_b == "b":
			new_val = exp2_eval - exp1_eval

		else: # Default value is "auto", so it goes to this
			new_val = exp1_eval - exp2_eval



		if new_val > 0:
			new_stat = "Positive"

		elif new_val < 0:
			new_stat = "Negative"

		dbg_funcs.debug("-"*30)
		dbg_funcs.debug(f"x = {x}")
		dbg_funcs.debug(f"x == x + jump: {x == x + jump}")
		dbg_funcs.debug(f"jump = {jump}")
		dbg_funcs.debug(f"expression1 eval: {exp1_eval}")
		dbg_funcs.debug(f"expression2 eval: {exp2_eval}")
		dbg_funcs.debug(f"new_val = {new_val}")
		dbg_funcs.debug(f"old_val = {old_val}")
		dbg_funcs.debug(f"new_stat = {new_stat}")
		dbg_funcs.debug(f"old_stat = {old_stat}")


		# "Winning" condition
		if new_val == D(0) or x == x + jump:
			answer = x

			dbg_funcs.debug("We have a winner!")
			dbg_funcs.debug(f"The winner is {x}")

			break



		elif new_stat != old_stat:
			x -= jump
			jump = jump / jump_divider

			dbg_funcs.debug("-"*30)
			dbg_funcs.debug("Lowering jump.")
			dbg_funcs.debug(f"x is now {x}")
			dbg_funcs.debug(f"jump is now {jump}")



		# This only runs if new_stat is old_stat, so
		# if a jump hasnt passed the "answer".
		if new_stat == old_stat:
			old_stat = new_stat
			old_val = new_val


	return format_layer1(str(answer))



def omega_iter_quad():

	# Iterative computation of Omega Constant
	# https://en.wikipedia.org/wiki/Omega_constant

	# The second one
	# Apperently has quadratic convergence


	n = D(0.5) # Initial guess


	while True:

		old = n
		n = D(1 + n) / D(1 + n.exp())

		a = str(old)[1:] # [1:] to remove "0."
		b = str(n)[1:]

		# Statusbar (only runs when there is a gurantee that we aren't done calculating)
		# This runs every round as the convergence is quadratic, so one round isn't
		# fast like in pi_chudnovsky, for example
		for k in range(1, len(a)):

			if a[:k] == b[:k] and a[:k+1] != b[:k+1]: # Find the difference
				dbg_funcs.debug(f"{len(b[:k])} digits calculated out of {getcontext().prec}")

		# In total, truncate 3 digits off of a and b. This is expected to not really
		# do anything, as extra_digits will compensate.
		if str(n)[:-2] == str(old)[:-2]:
			return format_layer1(str(n))

def omega_iter():

	# Iterative computation of Omega Constant
	# https://en.wikipedia.org/wiki/Omega_constant

	# The first one


	n = D(0.5) # Initial guess


	while True:

		old = n

		n = D(-n).exp()

		a = str(old)[1:] # [1:] to remove "0."
		b = str(n)[1:]

		# Statusbar
		for k in range(1, len(a)):

			if a[:k] == b[:k] and a[:k+1] != b[:k+1]: # Find the difference
				dbg_funcs.debug(f"{len(b[:k])} digits calculated out of {getcontext().prec}")


		# In total, truncate 3 digits off of a and b. This is expected to not really
		# do anything, as extra_digits will compensate.
		if str(n)[:-2] == str(old)[:-2]:
			return format_layer1(str(n))


# https://en.wikipedia.org/wiki/Omega_constant
# Cubic convergence, aka 3**k correct digits per k rounds
# It's slower than the quadratic one at basically any point,
# so it isn't default.
def omega_iter_cubic():

	n = D(0.5) # Initial guess
	new = 0

	while True:

		upper = n * D(n).exp() - 1


		lower1 = D(n).exp()*(n + 1)


		lower2_1 = (n + 2) * ( n * D(n).exp() - 1)
		lower2_2 = 2*n + 2

		lower2 = D(lower2_1) / D(lower2_2)


		div = D(upper) / D(lower1 - lower2)


		old = n
		n = n - div


		# Statusbar code from omega_iter_quad
		a = str(old)[1:]
		b = str(n)[1:]

		for k in range(1, len(a)):

			if a[:k] == b[:k] and a[:k+1] != b[:k+1]:
				dbg_tests.debug(f"{len(b[:k])} digits calculated out of {getcontext().prec}")


		if str(n)[:-2] == str(old)[:-2]:
			return format_layer1(str(n))

