#!/bin/sh
# uramanujanmaker.sh: Make uramanujan off of ramanujan

# Script to create a $NAME from ramanujan automatically

DIRPATH="$(realpath -- "$0" | sed "s/uramanujanmaker\.sh$//")"
NAME="${DIRPATH}uramanujan.py"

# Exit trap
cleanUp() {
	echo "Looks like something went wrong and the program somehow exited."
	echo "Cleaning up..."

	rm -f -- "$NAME"

	exit 1
}

trap cleanUp INT TERM


# Clean slate
rm -f -- "$NAME"

# Create "$NAME"
cat -- "${DIRPATH}vars.py" "${DIRPATH}debugging.py" "${DIRPATH}formatting.py" "${DIRPATH}ui.py" > "$NAME"

# Note: There is no need for families as it is only used in the UI.
# The only reason we keep ui.py (and delete the big while loop) is because
# the code to format the constant is in ui.

chmod +x -- "$NAME"

# Remove the part executing the import_stuff code
sed -i "/^exec/d" -- "$NAME"



# Beginning of UI loop line number
begin="$(awk '/Main option selection / {print FNR}' "$NAME")"

# End of the big UI while : (actually the debug info right before)
end="$(awk '/Debug info/ {print FNR - 2}' "$NAME")"
# The -2 is to compensate for the fact that it is actually searching
# for the debug info printing line

# Delete all lines between $beginloop and $endloop
sed -i "${begin},${end}d" -- "$NAME"



# Delete the format_const_to_calc function
begin="$(awk '/def format_const_to_calc/ {print FNR}' "$NAME")"
end="$(awk '/return fetch_pretty_family_name\(fetch_family_name\(const_to_calc\)\)/ {print FNR}' "$NAME")"

sed -i "${begin},${end}d" -- "$NAME"


# Fix up the things broken by removing format_const_to_calc

# Remove references
sed -i "/format_const_to_calc()/d" -- "$NAME"

# Fix a thing now broken with writing to a file
sed -i "/filename_name +=/s/Ramanujan/my_constant/g" -- "$NAME"



# The default constant is now my_constant, any code setting it to something
# else will be overridden
sed -i 's/^const_to_calc = .*/const_to_calc = "my_constant"/g' -- "$NAME"


# Make a simple definition for my_constant
sed -i "/getcontext().prec =/adef my_constant():" -- "$NAME"
sed -i "/def my_constant():/a \\	return \"123\"" -- "$NAME"
# (Having a \\ here seems to make it work)





# Remove more unused things
sed -i "/family/d" -- "$NAME"
sed -i "/fetch_algo/d" -- "$NAME"
sed -i "/hillclimb/d" -- "$NAME"
sed -i "/nth_root_of_x/d" -- "$NAME"
sed -i "/sqrt_n/d" -- "$NAME"
sed -i "/a_or_b/d" -- "$NAME"
sed -i "/jump_divider/d" -- "$NAME"
sed -i "/big_number/d" -- "$NAME"


# Remove the newlines function and it's uses
sed -i "/newlines/d" -- "$NAME"
sed -i "/\*100/d" -- "$NAME"




# Delete all comments that have their own line
sed -i "/^\t*#/d" -- "$NAME"

# Delete all comments on a line of code
sed -i 's/ \?#.*//g' -- "$NAME"


# Delete all empty lines, including those with just whitespace
sed -i "/^\t*$/d" -- "$NAME"


# Put a shebang in the beginning of the pogram
sed -i "1i#!/usr/bin/env python3" -- "$NAME"
