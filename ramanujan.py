#!/usr/bin/env python3
# ramanujan.py: The one to run

import sys

# This is officially the file to run when
# running the program.

# Make sure we are running the correct version of python
cur_version = sys.version_info
req_version = (3, 6)
pretty_version = str(req_version[0]) + "." + str(req_version[1])

if cur_version < req_version:
	raise RuntimeError("Ramanujan requires at least python " + pretty_version)


# Run ui.py

import sys, os
assert(sys.path[0] != "")

exec(open(os.path.join(sys.path[0], "ui.py")).read())
