# A simple makefile to make frankenrama and uramanujan, and remove them.

all:
	@echo "make frankenrama, or make uramanujan?"

# Hack that stops the make if frankenrama.py already exists
frankenrama: frankenrama.py
frankenrama.py:
	./frankenramamaker.sh

uramanujan: uramanujan.py
uramanujan.py:
	./uramanujanmaker.sh

clean:
	rm -f frankenrama.py uramanujan.py
	rm -rf __pycache__
