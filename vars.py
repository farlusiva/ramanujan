#!/usr/bin/env python3
# vars.py: Default varaibles are defined here

# File that handles default variables

from decimal import *
from math import factorial as fact
from textwrap import wrap
from time import time
import decimal
import sys

D = Decimal


# Check if gmpy2 is installed. If so, allow ramanujan to use it (it's quick).
# Otherwise, don't.
try:
	import gmpy2
	from gmpy2 import mpfr
	use_gmpy = True
except ModuleNotFoundError:
	use_gmpy = False


debug = False
initial_debug = 1
# Whether it is the first time the user enables debug in the program or not.
# Used to set the logging levels at first debug.


# Secret debug menu options:

big_number = 10**100
# Hillclimb v2 initial starting position, you could say.
# The starting point is negative this, so if it's 1000 then the lowest number you could find with it would be -1000.
# If it tries to find something smaller than the "starting point", it'd just go to positive infinity forever.
# Also the higher this number is the slower normal calculations are because
# if looking for say 1, it would have to divide "jump" by 10, wasting CPU cycles.
# Well, that's the price you pay for making something hard to break.

# By default, this is a googol simply because there is no forseeable way
# in which someone will need to find a number smaller than a negative googol.


extra_digits = 10
# Moved extra_digits to here so that it can be changed in the secret debug menu

series_extra_rounds = 5
# Extra rounds used in the code for infinite series calculating


a_or_b = "auto"
# Can be used in the secret dev menu
# to manually set a_or_b for use in hillclimbv1(v1).

jump_divider = 10
# What jump is to be divided by in the hillclimbs

disable_rounding = False
# Setting this to true will disable the rounding in format_layer1.
# To see the effects of this, try calculating the 3rd root of 125
# with and without rounding.



# Default settings:

digits_to_calc = 1000

print_or_write = "p" # p for print, w for write
formatting = True # Boolean, True for formatting, False for none.
line_length = 50 # Amount of digits per line
line_spacing = 10 # Amount of digits between spaces

# Constant to calculate
# Use the gmpy version if possible (it's quite quick)
const_to_calc = f"pi_chudnovsky{'_gmpy' if use_gmpy else ''}"


if formatting:
	length = int(digits_to_calc + extra_digits + line_length + 2)

else:
	length = int(digits_to_calc + extra_digits + 2)




nth_root_of_x_x = 2
nth_root_of_x_n = 2

sqrt_n_calc = 2


hillclimb_expression1 = "x**3"
hillclimb_expression2 = "x+1"

# Permanent variables for special consant_to_calculate's
# which require them. Technically they could be replaced
# with temp vars, but this makes it better, in my opinion.

