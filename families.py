#!/usr/bin/env python3
# families.py: Handles the family-related functions

# The family tree.
# Format: algo_name:[family, pretty algo name, uses gmpy?]
# Note: The name of the function must be equal to 'algo_name'.
#        as ui.py evals f"{const_to_calc}()" to calculate.
# Note: The order fetch_algo_family returns in is the order of appearance
#        in family_tree. Therefore, the algos should be shown in order
#        of which one is 'best'.

## TODO: Add prettier pretty names for plastic_{general, alternative{1,2}}.
family_tree = {
"pi_chudnovsky_gmpy"    :["pi",          "Chudnovsky (GMPY2)", True],
"pi_chudnovsky"         :["pi",          "Chudnovsky", False],
"pi_ramanujan"          :["pi",          "Ramanujan", False],
"pi_chudnovsky_alt"     :["pi",          "Alternative Chudnovsky", False],
"e_series"              :["e",           "Infinite series", False],
"e_general"             :["e",           "Decimal libary", False],
"zeta3_wedeniwski"      :["zeta3",       "Wedeniwski", False],
"zeta3_mohamud"         :["zeta3",       "Mohamud", False],
"golden_common"         :["golden",      "(1 + sqrt5) / 2", False],
"golden_alt"            :["golden",      "-2 / (1 - sqrt5)", False],
"nth_root_of_x"         :["nthrootofx",  "x ** ( 1 / n )", False],
"nth_root_of_x_log"     :["nthrootofx",  "10 ** ( log10(x) / n )", False],
"sqrt_general"          :["sqrt",        "Decimal Libary", False],
"sqrt_pow_half"         :["sqrt",        "n ** 1/2", False],
"sqrt_log"              :["sqrt",        "10 ** ( log10(n) / 2 )", False],
"plastic_general"       :["plastic",     "Unnamed", False],
"plastic_alternative1"  :["plastic",     "Alternative formula #1", False],
"plastic_alternative2"  :["plastic",     "Alternative formula #2", False],
"hillclimbv2"           :["hillclimb",   "Hillclimb engine v2", False],
"hillclimbv1"           :["hillclimb",   "Hillclimb engine v1", False],
"omega_iter_quad"       :["omega",       "Iterative (with refinement)", False],
"omega_iter"            :["omega",       "Iterative", False],
"omega_iter_cubic"      :["omega",       "Iterative (Halley's method)", False]
}

# Pretty family names. Used in format_const_to_calc via fetch_pretty_(...)
# It's defined here to expose it to the rest of the program as well.
pretty_family_list = {
"pi":"Pi",
"e":"e",
"zeta3":"Zeta(3) (Apery's constant)",
"golden":"Golden Ratio",
"nthrootofx":"nth root of x",
"sqrt":"Sqrt(n)",
"plastic":"Plastic Constant",
"hillclimb":"Hillclimb",
"omega":"Omega Constant"
}


def fetch_family_name(to_fetch):
	# Fetch the family name from a known "algo_name"
	# Used to find the alternative algos

	return family_tree[to_fetch][0]


def fetch_algo_family(to_fetch):
	# Fetches list of family members of known family

	family = []

	for k in family_tree:
		if family_tree[k][0] == to_fetch:

			# Filter out gmpy if it's not enabled
			if not use_gmpy and family_tree[k][2] == True:
				continue
			family.append(k)

	return family


def fetch_algo(to_fetch):
	# Fetches algorithm "name" for printing use

	return family_tree[to_fetch][1]

def fetch_pretty_family_name(family_name):
	return pretty_family_list[family_name]
