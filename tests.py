#!/usr/bin/env python3
# tests.py: Tests

import os
import sys

assert(sys.path[0] != "")
exec(open(os.path.join(sys.path[0],"import_stuff.py")).read())

from optparse import OptionParser
from random import randint
import subprocess

try:
	import gmpy2
	from gmpy2 import mpfr
	use_gmpy = True
except ModuleNotFoundError:
	use_gmpy = False

# Manually set a few settings for testing

usage="usage: %prog [options]"
parser = OptionParser(usage=usage)

parser.add_option(
	"--debug",
	action="store_true",
	dest="debug",
	help="Enable debug mode",
	default=False,
	)

parser.add_option(
	"-d", "--digits",
	type="int",
	dest="digits_to_calc",
	help="Floating-point precision",
	default=100,
	)

parser.add_option(
	"-r", "--range",
	type="int",
	default=1000,
	dest="range_tests",
	help="Bigger number = more testing",
	)

parser.add_option(
	"-u", "--nounix", "--no-unix",
	default=True,
	dest="test_unix",
	action="store_false",
	help="Disable unix tests",
	)

parser.add_option(
	"--dfdm",
	default=False,
	action="store_true",
	dest="disable_fl1_debug_msgs",
	help="Disable formatting_layer1 debug messages",
	)

parser.add_option(
	"-f", "--disable-formatting",
	dest="formatting",
	action="store_false",
	default=True,
	help="Disable formatting",
	)

(options, args) = parser.parse_args()

# At this point, "options" is a dictionary or something
# so, "options.debug" is the debug variable, not "debug".

# I'm lazy so instead of parsing it, the variables are manually fixed.
# Because of this, you have to fix the variables down here whenever adding more
# options.


debug = options.debug
range_tests = options.range_tests
digits_to_calc = options.digits_to_calc
disable_fl1_debug_msgs = options.disable_fl1_debug_msgs
formatting = options.formatting

dbg_tests = setup_logger("dbg_tests"); # for tests
list_loggers.append(dbg_tests)

if debug:
	dbg_tests.setLevel(logging.DEBUG)
	dbg_funcs.setLevel(logging.DEBUG)
	dbg_dfdm.setLevel(logging.DEBUG)

if disable_fl1_debug_msgs:
	dbg_dfdm.setLevel(logging.WARNING)


# Check the OS so we can set has_unix to it's proper value
if options.test_unix:
	# --nounix was not given, test os by default

	dbg_tests.debug("Testing OS whether unix-like or not")
	dbg_tests.debug(f"OS is \"{os.name}\"")


	# Boolean magic
	has_unix = os.name == "posix"

	if has_unix:
		print("Detected unix-like OS, will do unix tests")

	else:
		print("Didn't detect unix-like OS, will not do unix tests")


# not options.test_unix *and* debug is enabled
else:

	print("The unix tests have been overridden and will not be run")
	has_unix = False

# Needs to be true for the formatting digits test, as extra calculation digits
# are required for it to format properly.
# If you set it to false, it will only format the first 50 digits.

if formatting:
	length = int(digits_to_calc + extra_digits + line_length + 2)
else:
	length = int(digits_to_calc + extra_digits + 2)

getcontext().prec = length


# Shell functions because the entire thing takes too much space.
# Also easier to change afterwards if, for by some reason they need to be changed.
def shell(inp):
	os.system(inp)

def shell_output(inp):
	return subprocess.check_output(inp, shell=True)


# Simple rounding test
if not disable_rounding:
	assert(nth_root_of_x(3, 125) == nth_root_of_x_log(3, 125) == "5")


	# Should be the same
	temp = "2.1" + "0" * (length - len("2.1") - extra_digits)
	assert(sqrt_general("4.41") == sqrt_pow_half("4.41") == sqrt_log("4.41") == temp)


# Hilllimb 1/8 test, mainly just checks if the amount of zeroes are correct
temp = "0.125" + "0" * (length - len("0.128") - extra_digits)
assert(hillclimbv2("1/8", "x") == temp)




# Should match pi_chudnovsky() when digits_to_calc is 100
pi_test = """3.
1415926535 8979323846 2643383279 5028841971 6939937510 | 50
5820974944 5923078164 0628620899 8628034825 3421170679 | 100
"""

# Formatting test
# This is hardcoded to 100 because i'm lazy so it will only run when
# digits_to_calc is 100 (default).

if digits_to_calc == 100 and formatting:
	assert(format_digits(pi_chudnovsky()) == pi_test)

# Not digits_to_calc == 100 AND debug is True
else:

	dbg_tests.debug("Not doing the formatting test because digits_to_calc is not 100.")


# Pi
assert(pi_chudnovsky() == pi_ramanujan())
assert(pi_chudnovsky() == pi_chudnovsky_alt())
if use_gmpy:
	gmpy2.get_context().precision = int(length*3.322)
	assert(pi_chudnovsky() == pi_chudnovsky_gmpy())


# nth root of x
# Need a special test for constants that take input
# Should work on all non-square / non-cube etc numbers
for k in range(range_tests):

	n = randint(1,10000)/100 # Dividing by 100 to allow for some float testing
	x = k


	a = nth_root_of_x_log(n, x)
	b = nth_root_of_x(n, x)

	dbg_tests.debug("")
	dbg_tests.debug(f"x, n = {x}, {n}")
	dbg_tests.debug(f"log  = {a}")
	dbg_tests.debug(f"norm = {b}")
	dbg_tests.debug("")

	assert(a == b)


# Zeta(3)
assert(zeta3_mohamud() == zeta3_wedeniwski())

# e
assert(e_general() == e_series())

# Plastic constant
assert(plastic_general() == plastic_alternative1())
assert(plastic_general() == plastic_alternative2())

# Golden Ratio
assert(golden_common() == golden_alt())

# Omega Constant

# WARNING: omega_iter is SUUPER slow
# Because of this, the precision is temporarily lowered!
# (500 more or less is fine, anything above however, is a crawl like hillclimb.)
if getcontext().prec > 500:
	getcontext().prec = 500

assert(omega_iter_quad() == omega_iter() == omega_iter_cubic())

getcontext().prec = length
gmpy2.get_context().precision = int(length*3.322)


# turn_into_decimal
assert(turn_into_decimal(-1) == D(1))
assert(turn_into_decimal(1) == D(1))
assert(turn_into_decimal("") == D(0))
assert(turn_into_decimal("abcdef") == D(0))
assert(turn_into_decimal("123....4") == D("123.4")) # Note that D(123.4) is not always the same as D("123.4")
assert(turn_into_decimal("...1...2...3...") == D("0.123"))



# Square roots
for k in range(range_tests):

	square = randint(1, 100000)

	a = sqrt_general(square)
	b = sqrt_pow_half(square)
	c = sqrt_log(square)


	dbg_tests.debug(f"a = {a}")
	dbg_tests.debug(f"b = {b}")
	dbg_tests.debug(f"c = {c}")

	assert(a == b)
	assert(b == c)

# Family tings

assert(fetch_family_name("golden_common") == fetch_family_name("golden_alt"))
assert(fetch_family_name("pi_chudnovsky") == "pi")
assert(fetch_family_name("e_general") != "pi")
assert(fetch_family_name("e_general") == "e")
assert(fetch_algo_family("pi") == ["gmpy_pi_chudnovsky", "pi_chudnovsky", "pi_ramanujan", "pi_chudnovsky_alt"])
assert(fetch_pretty_family_name("pi") == "Pi")


# Requires the has_unix variable to be True, so it can be easily disabled
# when writing on a system that is not unix-based (Shocking, i know)

if has_unix:

	# Uramanujanmaker test
	shell(f"{sys.path[0]}/uramanujanmaker.sh")

	# Results of running urama
	try:
		uramamakerresults = str(shell_output(f"echo '\n' | {sys.path[0]}/uramanujan.py"))
		shell(f"rm {sys.path[0]}/uramanujan.py")

	except:
		dbg_tests.critical("There seems to have been a error message in uramanujanmaker. Uramanujan.py will not be deleted")
		raise SystemExit


	shell(f"{sys.path[0]}/frankenramamaker.sh")
	# Results of running uramamaker
	try:
		uramamakerresults = str(shell_output(f"echo '0\n0' | {sys.path[0]}/frankenrama.py"))
		shell(f"rm {sys.path[0]}/frankenrama.py")

	except:
		dbg_tests.critical("There seems to have been a error message in frankenrama. Frankenrama.py will not be deleted.")
		raise SystemExit



	# Shell based test:
	# Tests to see if a) Ramanujan can write to a file and b) can do non-formatted digits
	# Also tests ui to see if it works

	digits = randint(5, digits_to_calc)
	# Can go higher by increasing digits_to_calc

	# Calculate digits
	shell(f"echo '3\n{digits}\n4\n5\n0\n' | {sys.path[0]}/ramanujan.py > /dev/null")
	# Redirect all output to /dev/null so the shell running tests won't get
	# spammed with the user interface

	# The pi ramanujan file is in the runner's folder by purpose,
	#  so dont' use sys.path[0] here.
	amount = int(turn_into_decimal(shell_output("wc -m 'Pi - Ramanujan.txt' | awk '{print $1}'")))
	# Use awk to extract amount of characters,

	dbg_tests.debug("Doing the shell-based ui test")
	dbg_tests.debug("If all goes well, amount should be two more than digits.")
	dbg_tests.debug(f"Amount, digits = {amount}, {digits}")


	assert(amount - 2 == digits)
	# -2 is to compensate for "3."

	# This assert will only work if Ramanujan returned the *exact* amount of digits
	# given by the user. (randomly chosen in this case)

	shell("rm 'Pi - Ramanujan.txt'")
	# Cleanup


	dbg_tests.debug("Doing shell-based rounding test.")
	dbg_tests.debug("The is designed so it will fail if the result")
	dbg_tests.debug("has many nines after the decimal")

	for k in range(1, int(range_tests / 20) + 2):
		# Another shell-based test.
		# This one will try to calculate the root of a random cube, to make sure rounding works.
		# If it does, the result should be 5. (When rounding is disabled, it will be 4.999...)

		# Therefore, this test will fail if rounding is disabled in the debug menu.

		## This test does less testing because it is relatively slower than
		## the others


		# Cube root and cube (uses range_tests as highest number so it is not
			# an arbitrary limit)
		cubert = randint(1, range_tests + 1)
		cube = cubert ** 3

		# The one below will fail because it turns off rounding
		#shell(f"echo '1\n5\n3\n{cube}\n4\n5\ndebug\n99\n7\nTrue\n\n0\n' | {sys.path[0]}/ramanujan.py > /dev/null")

		shell(f"echo '1\n5\n3\n{cube}\n4\n5\n0\n' | {sys.path[0]}/ramanujan.py > /dev/null")

		result = open(f"3rd root of {cube} - Ramanujan.txt", "r").read()

		dbg_tests.debug(f"Expected, result = {cubert}, {result}")


		assert(str(cubert) == result)

		# Cleanup
		shell(f"rm '3rd root of {cube} - Ramanujan.txt'")



# No has_unix, but debug is True
else:
	dbg_tests.debug("Not doing unix tests because has_unix is False")

print("If you are reading this, then all tests probably passed.")
