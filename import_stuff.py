#!/usr/bin/env python3
# import_stuff.py: Used to import code in a way to keep variables

# Import all code from other files, so you only
# need to run one file.

# os and sys must be imported before running this file.
# Run with:
# exec(open(os.path.join(sys.path[0],"import_stuff.py")).read())
# as it allows you to run ramanujan.py in another directory as ramanujan.py.
# The same goes for ui.py and tests.py.


files = ["vars.py", "funcs.py", "families.py", "formatting.py", "debugging.py"]

for k in files:
	exec(open(os.path.join(sys.path[0], k)).read())
