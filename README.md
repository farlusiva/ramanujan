# Ramanujan


## About

Ramanujan is a program to calculate digits of various mathematical constants
such as π, e and √n.

## Requirements

### For running Ramanujan
Python 3.6 or newer

### For using the scripts
Any largely POSIX-compliant system should do

### For the tests
You **must** have the required python version (3.6, or whatever it says) as
your "python" executable, as the tests uses the shebang to run the scripts.

This could be worked around by replacing all the  shebangs with "python3.6",
or something equivalent.

## Usage

__`./ramanujan.py`__

## Comparison to y-cruncher

ramanujan's pros:

* Free! (In both meanings of the term)
* More formulas (for some constants, y-cruncher always has two. Ramanujan? Two *or more*)
* Debug mode!
* Arguably better UI that is definently not stolen from y-cruncher \*cough cough\*
* Can print digits directly to console
* Can correctly compute the root of 364771411312262581333558204925425280470042756
* Can round numbers (i.e. 3.999... -> 4 and 3.000... -> 3)
* Can solve equations (okay, *try* to solve)
* Digit formatting that doesn't use hardcoded variables

y-cruncher's pros:

* Speed
* Can calculate offsets of pi (BBP formula)
* Serious speed
* Can benchmark/stress test components and IO
* Multiple binaries optimised for speed
* Multithreading
* Did I mention speed?
* Writes hex digits
* Actually used by more than one person
* Builtin swap support
* Pretty colours


## Known bugs

1.  Calculating things with absurdly large integer parts (length of int is bigger
than the extra\_digits buffer) will not format the correct amount of digits.  
This seems to caused by the Decimal library probably regarding "digits" as
'digits after the decimal point when written in scientific notation'; while
format\_digits implies that "digits" refer to the ones after the decimal point.  
This is evident when trying to calculate 1000 digits of the 0.003th root of 5.  
One possible fix for this would be to show all constants in scientific notation.

## Files

### vars.py

Here lie most of the default variables.


### funcs.py

Defines functions that calculate constants.


### ui.py

This is where the user interface is.

Most of the code is here, as it used to hold all the code when all of ramanujan
was a single file.

### families.py

Basically the family tree for which algorithms calculate what constant.

### ramanujan.py

When you run ramanujan, you run ramanujan.py.

### tests.py

Tests holds all the automated tests.

### import\_stuff.py

Imports all ramanujan-related code like the default variables, functions et al.

## uramanujanmaker.sh

uramanujanmaker.sh will automatically create a version of ramanujan called
"uramanujan", which is basically a barebones ramanujan without the UI, nor any
constants to calculate. uramanujan was designed to be a enviorment to write
new constant's to calculate without having to add it in the UI, deal with
family problems and having to set the same calculating settings every single time.

To actually make the newly created uramanujan.py do something, you'll have to
edit the function named "my\_constant", which is somewhere in uramanujan.
By default, it returns an empty string.

## frankenramamaker.sh

Frankenramamaker is a shell script that makes a version of ramanujan, similar
to that of uramanujanmaker, which is called frankenrama.py
Frankenrama is as functional as the normal ramanujan, but it is written
in one line of code. This was done to prove that any python code can be turned
into one line of code. Just for fun of course.


## Extra stuff / Tips

If you are curious on how many lines of code ramanujan totals, (incl scripts)
you can run this to find out.

__`sed -e "/^\s*#/d" -e "/^$/d" *.py *.sh | wc -l`__

It should work most of the time.

You will need to delete uramanujan.py and frankenrama.py if you want an accurate reading


### One file

If you want to turn all of the ramanujan files into a single python file for
portability, there's a simple one-liner for that too.

__`cat vars.py f*.py debugging.py ui.py | sed "/^exec/d" > output.py`__


### Echo stdin to ramanujan

If you want to run ramanujan with the same settings multiple times without having
to manually set the same settings all the time, you can easily echo stdin to ramanujan,
where a newline character (\n) is like pressing enter. An example, if you want
to calculate the 3rd root of 125 without rounding (rest is default), you could
do something like:

__`echo "debug\n99\n7\nTrue\n0\n1\n5\n3\n125\n0\n0" | ./ramanujan.py`__

A simpler example, which presses zero once (all default), would be:

__`echo "0\n" | ./ramanujan.py`__
