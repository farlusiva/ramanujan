#!/bin/sh
# frankenramamaker.sh: Make frankenrama


# Note for later:
# Frankenramamaker was originally supposed to make every string
# a triple-quote string, but had complications with the
# "There is a '.' in ..." string (and similar ones)
# Because of this, frankenramamaker instead turns every string into
# a f-string.

DIRPATH="$(realpath -- "$0" | sed "s/frankenramamaker\.sh$//")"
NAME="${DIRPATH}frankenrama.py"


# Exit trap
cleanUp() {
	echo "Looks like something went wrong and the program somehow exited."
	echo "Cleaning up..."

	rm -f -- "$NAME"

	exit 1
}

trap cleanUp INT TERM


# Clean slate
rm -f -- "$NAME"

cat -- "${DIRPATH}vars.py" "${DIRPATH}"f*.py "${DIRPATH}"debugging.py "${DIRPATH}ui.py" > "$NAME"
chmod +x -- "$NAME"

# Remove the exec thing and the things related
sed -i "/^exec/d" -- "$NAME"
sed -i "/^assert(sys.path[0]/d" -- "$NAME"

# Delete all comments that have thier own line
sed -i "/^\t*#/d" -- "$NAME"

# Delete all comments on a line of code (incl. proceeding space)
sed -i 's/ \?#.*//g' -- "$NAME"


# Delete all whitespace
sed -i "/^$/d" -- "$NAME"


# Fix things broken by the comment remover

sed -i 's/^"plastic_alternative\([12]\)"\( *\):\["plastic",\( *\)"Alternative formula$/"plastic_alternative\1"\2:["plastic",\3"Alternative formula #\1"],/g' -- "$NAME"

sed -i 's/print(\(f\?\)"$/print(\1"#"*30)/g' -- "$NAME" # Will match f-strings and preserve them as well
sed -i 's/\(the constant to calculate\)$/\1#: ")/g' -- "$NAME"
sed -i 's/\(algorithm\)$/\1 #: ")/g' -- "$NAME"
sed -i 's/\(Choose an option\)$/\1 #: ")/g' -- "$NAME"
sed -i 's/\(select the option\)$/\1 #)\\n\\n")/g' -- "$NAME"
sed -i 's/\(Choose the constant to calculate\)$/\1 #: )\\n\\n")/g' -- "$NAME"
sed -i 's/\(Select the new algorithm\)$/\1 #: )\\n\\n")/g' -- "$NAME"



# Turn every string into a f-string
sed -i 's/\([^f\\]\)"\([^, *:)]\)/\1f"\2/g' -- "$NAME"
# Do not ever change this regex.
# Trust me.



# Fix a thing that is broken by the above command, yet cannot be fixed in it.
# The problem is i also need a literal ] inside the second \( \), but if i
# add a \], sed will think that closes the one opened.

sed -i 's/f"]/"]/g' -- "$NAME"

# Replace every literal newline with a {newline} (requires the f-string trick)
sed -i 's/\\n/{newline}/g' -- "$NAME"


# Define the newline variable
sed -i "1inewline = '''\n'''" -- "$NAME"
# Because this is the only triple-quote string aside from the exec, one of them
# needs to be a " triple-quote string and the other a ' triple-quote string.



# Python thinks a string like this is invalid, so below is a fix for that
# "abc \" <-- Double-quote in double-quote string! \" End of string -->"
sed -i "s/\\\\\"/'/g" -- "$NAME"


# Add the exec
sed -i '1iexec("""' -- "$NAME"
sed -i '$a""")' -- "$NAME"


# Replace all newlines with a literal "\n"
# Thank Stackoverflow for this
sed -i -e ':a' -e 'N' -e '$!ba' -e 's/\n/\\n/g' -- "$NAME"


# Fix the whitespace (may be optional)
sed -i 's/\t/   /g' -- "$NAME"


# Delete a literal "\" which somehow manged to get to the EOL
sed -i 's/\\")\\$/")/g' -- "$NAME"


# Readd the shebang
sed -i "1i#!/usr/bin/env python3" -- "$NAME"
