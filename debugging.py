#!/usr/bin/env python3
#
# Logging

import logging
logging_format = '%(name)s %(levelname)s(%(levelno)s): %(filename)s:%(lineno)d:%(funcName)s: %(message)s'

# Sets up a logger.
# stackoverflow.com/questions/11232230/logging-to-two-files-with-different-settings
def setup_logger(name, level=logging.WARNING):

	logger = logging.getLogger(name)
	logger.setLevel(level)

	return logger


logging.basicConfig(level=10, format=logging_format)

dbg_funcs = setup_logger("dbg_funcs") # funcs.py
dbg_dfdm = setup_logger("dbg_dfdm") # format_layer1
dbg_ui = setup_logger("dbg_ui") # ui.py

list_loggers = [dbg_funcs, dbg_dfdm, dbg_ui]
