#!/usr/bin/env python3
# ui.py: The user interface
# http://www.craig-wood.com/nick/images/math/40f2e34873e611fb04c7b93ed846b724.png
# (The middle formula is used for pi_chudnovsky_alt)

# Remember to add new algos to family_tree when added.

import sys
import os
assert(sys.path[0] != "")
# Must be done before execing import_stuff, __location__ will persist
#  in import_stuff, as it's execed.
# realpath fixes symlinks,


exec(open(os.path.join(sys.path[0], "import_stuff.py")).read())
# Executing the file overrides the shared variable problem without having to
# specify that every single variable is global




def vardump(print_func):
	# Basically just printing lots of vars for debug use
	# It is a function because it is used in debug logs
	# and in the secret debug menu
	# print_func: the function to print with.

	print_func(f"formatting: {formatting}")
	print_func(f"print_or_write: {print_or_write}")
	print_func(f"line_spacing: {line_spacing}")
	print_func(f"line_length: {line_length}")
	print_func(f"digits_to_calc: {digits_to_calc}")
	print_func(f"const_to_calc: {const_to_calc}")
	print_func(f"Family members: {fetch_algo_family(fetch_family_name(const_to_calc))}")
	print_func(f"Family name: {fetch_family_name(const_to_calc)}")
	print_func(f"Algorithm name: {fetch_algo(const_to_calc)}")
	print_func(f"Formatted const_to_calc: {format_const_to_calc()}")
	print_func(f"disable_rounding: {disable_rounding}")



def newlines():
	# NOTE: assumed vt100. ^[A^[K -> move up, delete.
	# See this: climagic.org/mirrors/VT100_Escape_Codes.html
	if debug:
		print("-"*80)
	else:
		print('\x1b[A\x1b[K'*100, end="")


# End function definitions,
# begin ui loop.



while True: # Main option selection loop
	newlines() # Newlines help create the illusion that what has already been printed has been changed


	print("Current settings: (select the option # to change)\n\n")


	print(" "*5 + "1" + " "*10 + f"Constant to calculate: {format_const_to_calc()}\n")

	print(" "*5 + "2" + " "*10 + f"Algorithm: {fetch_algo(const_to_calc)}\n")

	print(" "*5 + "3" + " "*10 + f"Digits to calculate: {digits_to_calc}\n")

	print(" "*5 + "4" + " "*10 + "Print or write digits: ", end="")

	if print_or_write == "p":
		print("Print\n")
	elif print_or_write == "w":
		print("Write\n")



	print(" "*5 + "5" + " "*10 + "Formatting: ", end="")


	if formatting:
		print("Format digits\n")

		print(" "*5 + "6" + " "*10 + f"Line length (digits per line): {line_length}" + "\n")

		print(" "*5 + "7" + " "*10 + f"Line spacing (digits between each space): {line_spacing}\n")


	else:
		print("Do not format digits\n")


	if debug:
		print(" "*5 + "99" + " "*9 + "Secret debug menu\n")


	print("\n" + " "*5 + "0" + " "*10 + "Start computation\n")


	option = input("Choose an option #: ").lower()


	if option == "debug":
		debug = not debug

		# Set the logging levels to 'debug'
		if initial_debug:
			for k in list_loggers:
				k.setLevel(logging.DEBUG)
			initial_debug = 0


	if option == "1":
		while True:
			newlines()

			print(f"Current constant to calculate: {format_const_to_calc()}\n")

			# Generate the list from the pretty family names instead of printing
			#  it manually.
			for k in enumerate(["sqrt", "golden", "e", "pi", "nthrootofx",
			           "plastic", "zeta3", "hillclimb", "omega"]):

				tmp = str(k[0] + 1)
				lentmp = len(tmp)
				print(" "*(6-lentmp) + tmp + " "*10 + pretty_family_list[k[1]] + "\n")



			option = input("\nChoose the constant to calculate #: ").lower()

			# Leave const_to_calc unchanged if no input is given
			if option == "":
				break

			if option == "1" or option == "sqrt(n)":
				const_to_calc = "sqrt_general"
				sqrt_n_calc = get_pos_num("n = ")
				break


			elif option == "2" or option == "golden ratio":
				const_to_calc = "golden_common"
				break

			elif option == "3" or option == "e":
				const_to_calc = "e_series"
				break

			elif option == "4" or option == "pi":
				const_to_calc = f"pi_chudnovsky{'_gmpy' if use_gmpy else ''}"
				break


			elif option == "5" or option == "nth_root_of_x":
				nth_root_of_x_n = get_pos_num("n = ")
				nth_root_of_x_x = get_pos_num("x = ")
				const_to_calc = "nth_root_of_x"
				break

			elif option == "6" or option == "plastic constant":
				const_to_calc = "plastic_general"
				break

			elif option == "7" or option == "zeta(3)":
				const_to_calc = "zeta3_wedeniwski"
				break


			elif option == "8" or option == "hillclimbv1":
				print("\nThis is a beta. Because of this, do not expect the result to be correct.")
				print("This hillclimb works as a hillclimb by mathematical identities with one variable.")
				print("This means that if you write in x**3 = x+1, then you will get the Plastic constant.")
				print("When writing in parts, it goes like this: a = b. If you are using variables other than")
				print("x, such as rasing e to the power of x, refer to the Python Decimal libary for the")
				print("Python version you are running. An example for raising e to the power of x is decimal.Decimal(x).exp()")
				print("Also, if you want to write in something like decimal.Decimal(x), you can instead")
				print("just use D(x). So if you want to raise e to the power of x, you can instead just")
				print("use D(x).exp()")


				print("\nConfirmed things that can be calculated:")
				print("Plastic constant: x**3 = x+1")
				print("Omega constant: x*x.exp() = 1")

				print("\n\n\n*** Please do not use hillclimbv1. It will only cause headaches and probably give you***")
				print("*** an error about dividing by zero here and there when hillclimbv2 has no problem. ***\n")


				const_to_calc = "hillclimbv2"

				hillclimb_expression1 = input("a = ")
				hillclimb_expression2 = input("b = ")
				break

			elif option == "9" or option == "omega constant":
				const_to_calc = "omega_iter_quad"
				break




	elif option == "2":
		# Dynamic selecting code allows for
		# family lists to be of any length
		# Of course, as long as memory allows for it.

		newlines()


		print(f"Current algorithm for {format_const_to_calc()}: {fetch_algo(const_to_calc)}")
		print("Generally speaking, the first algorithm is the fastest one\n")


		family_list = fetch_algo_family(fetch_family_name(const_to_calc))

		for k in range(len(family_list)):
			print(" "*5 + str(k+1) + " "*10 + fetch_algo(family_list[k]) + "\n")

		print("")


		while True:
			option = input("Select the new algorithm #: ")

			# Don't change the algorithm if no input is given
			if option == "":
				break


			option = int(turn_into_decimal(option))


			# Option must be larger than 0 because we are counting from 1.
			# Also, counting 1 from makes it so string that don't contain
			# any numbers (when given to turn_to_decimal return 0) will not
			# accidentally select the 0th item.
			if D(option) == int(option) and 0 < int(option) <= len(family_list):

				temp = family_list[option - 1]
				# option - 1 because we are counting from 1


				const_to_calc = temp
				break




	elif option == "3": # Digits to calculate

		newlines()
		print(f"Current amount of digits to calculate: {digits_to_calc}\n\n")

		while True:
			option = input("Choose how many digits to calculate: ")

			# Avoid changing digits_to_calc if user gives no input
			if option == "":
				break

			option = int(turn_into_decimal(option))
			# Turning option into an int after turn_into_decimal
			# is so that it won't try to calculate 0.1 digits which it could
			# when it was only checking if option was bigger than 0.

			if option >= 1:
				digits_to_calc = option
				break




	elif option == "4": # Swap between print or write

		if print_or_write == "p":
			print_or_write = "w"

		elif print_or_write == "w":
			print_or_write = "p"


	elif option == "5": # Swap between formatting on or off
		formatting = not formatting



	elif option == "6" and formatting: # Line length
		newlines()

		print("Note: The line length will only be used if formatting the digits.\n")
		print(f"Current line length: {line_length}\n\n")


		while True:
			option = input("Choose the line length: ")

			# Leave line_length unchanged if no input given
			if option == "":
				break


			option = int(turn_into_decimal(option))

			if option > 0:
				line_length = option
				break



	elif option == "7" and formatting: # Line spacing
		newlines()

		print("Disabled if set to 0.\nIt is recommended to use a line space number that is divisible by the line length.\n")
		print(f"Current line spacing: {line_spacing}\n\n")

		option = input("Choose the line spacing: ")

		# Leave line_spacing unchanged if no input given
		if option != "":

			option = int(turn_into_decimal(option))

			line_spacing = option


	elif option == "99" and debug:

		while True:
			newlines()

			print("Welcome to the secret debug menu. From here you can change variables")
			print("which are usually unchangable. You must be careful however, as there")
			print("is no failsafe that makes sure everything will still work if you change something.")
			print("If you want to go out of debug mode, type \"debug\" as you did to get here.")
			print("Any changes done here will be kept if debug mode is turned off.\n\n\n")


			print(" "*5 + "1" + " "*10 + f"extra_digits: {extra_digits}\n")

			print(" "*5 + "2" + " "*10 + f"big_number (len): {len(str(big_number))}\n")

			print(" "*5 + "3" + " "*10 + f"series_extra_rounds: {series_extra_rounds}\n")

			print(" "*5 + "4" + " "*10 + f"a_or_b: {a_or_b}\n")

			print(" "*5 + "5" + " "*10 + f"jump_divider: {jump_divider}\n")

			print(" "*5 + "6" + " "*10 + f"Logging levels\n")

			print(" "*5 + "7" + " "*10 + f"disable_rounding: {disable_rounding}\n")

			print(" "*5 + "99" + " "*9 + "Print debug info\n")

			print(" "*5 + "0" + " "*10 + "Go back\n")


			option = input("Choose an option #: ").lower()



			if option == "1" or option == "extra_digits":
				newlines()

				print("extra_digits is a variable that is used to control the amount of \"extra\"")
				print("which could also be called failsafe digits, which are calculated ontop of the")
				print("desired precision to make sure that all digits in the desired precision")
				print("will be correct." + "\n" * 5)


				print(f"Current: {extra_digits}\n")


				extra_digits = get_pos_num("Choose what to change it to: ")




			elif option == "2" or option == "big_number":
				newlines()

				print("big_number is a variable used as the starting position for hillclimbv2.")
				print("The starting position is actually the negative of big_number, but that is so")
				print("you can enter big numbers here. If you set big_number to say, -1, then hillclimbv2")
				print("will start at --1 (+1), and if the answer is something less than that, it'll just")
				print("go on to plus infinity forever.\n")

				print("Please note that eval will be used so that you can enter nice powers of ten easily." + "\n"*5)


				print(f"Current (val): {big_number}")
				print(f"Current (len): {len(str(big_number))}\n")




				big_number = eval(input("Choose what to change it to (eval): "))


			elif option == "3" or option == "series_extra_rounds":
				newlines()

				print("series_extra_rounds is a variable used to determine the amount of")
				print("extra rounds used by infinite series formulas to calculate thier constant.")
				print("Currently used by pi_chudnovsky, pi_ramanujan and zeta3_wedeniwski and zeta3_mohamud")
				print("series_extra_rounds is not used for infinite series where there is no")
				print("pre-specified amount of rounds to calculate (probably just because i have")
				print("no idea how fast they converge). An example of this is ecalc_series." + "\n" * 5)

				print(f"Current: {series_extra_rounds}\n")


				series_extra_rounds = get_pos_num("Choose what to change it to: ")


			elif option == "4" or option == "a_or_b":
				newlines()

				print("a_or_b is a variable used to change how")
				print("the hillclimbs work, and when calculating")
				print("it is crucial to be correct for hillclimbv1")
				print("Hillclimbv2 was designed without the need for")
				print("a a_or_b variable, but was later given the")
				print("ability anyways. By default a_or_b is \"auto\"")
				print("When a_or_b is auto, hillclimbv1 will test with")
				print("a_or_b being a, and b to see which is correct")
				print("and then use that. Hillclimbv2 will interpret")
				print("it as a.\n")
				print("a_or_b decides which user-inputted expression")
				print("is reduced by the other. If a_or_b is a,")
				print("it will be expression1 - expression2, where")
				print("exoression1 and 2 are the first and second")
				print("user-inputted expressions" + "\n" * 5)

				print(f"Current: {a_or_b}\n")


				while True:

					tmp = input("Choose what to change it to: ").lower()

					if tmp in ["auto", "a", "b"]:
						a_or_b = tmp
						break


			elif option == "5" or option == "jump_divider":
				newlines()

				print("jump_divider is the value of what jump will")
				print("be divided by if the eval expressions are")
				print("positive and becomre negative, and vice-versa.")
				print("That means every time jump is reduced, it is")
				print("divided by jump_divider." + "\n"*5)


				print(f"Current: {jump_divider}\n")


				jump_divider = get_pos_num("Choose what to change it to: ")


			elif option == "6":
				newlines()

				print("Here you can set the logging levels for the loggers")
				print("that ramanujan uses.\n")

				print("Current:\n")

				for k,l in enumerate(list_loggers):
					print(" "*5 + str(k+1) + " "*10 + f"{l.name}: {l.level}\n")


				while True:
					tmp = input("\nPick a logger to change its value: ")

					if tmp == "":
						break

					tmp = int(tmp)

					while True:
						if 1 <= tmp <= len(list_loggers):

							picked = list_loggers[tmp-1]
							val = input(f"Set the level for {picked.name}: ")

							if val == "":
								continue

							val = int(val)
							if 0 <= val <= 50:
								picked.setLevel(val)
								break


					break


			elif option == "7" or option == "disable_rounding":
				newlines()

				print("disable_rounding is a boolean that will disable the rounding")
				print("in format_layer1 if enabled. To see an example of this,")
				print("try to calculate the 3rd root of 125 with and without")
				print("this setting turned on." + "\n" * 5)

				print(f"Current: {disable_rounding}\n")


				print("Press enter to swap the rounding. ", end="")
				tmp = input()

				disable_rounding = not disable_rounding



			elif option == "99":
				newlines()

				vardump(print)


				temp = input("\nPress enter to go back: ")



			elif option == "0" or option == "":
				break


	elif option == "0":
		newlines()
		break


# Print some variables
dbg_ui.debug("Debug info: ")
vardump(dbg_ui.debug)


if formatting:
	length = int(digits_to_calc + extra_digits + line_length + 2)

else:
	length = int(digits_to_calc + extra_digits + 2)
# The extra 2 is to compensate for the "3." when printing or writing unformatted digits


# Also the int() part is because the turn_to_int function returns a D(),
# and digits_to_calc goes through it to stop things like the sqrt of -1.



getcontext().prec = length # Sets precision for calculating with the decimal libary
if use_gmpy:
	# the magic number is log2(10), a scaling number to make decimal precision
	# into binary precision
	gmpy2.get_context().precision = int(length*3.322)
sys.set_int_max_str_digits(max(2*(length+100),640)) # should be safe




start_time = time()

# This is where the calculation happends
constant = eval(f"{const_to_calc}()")

stop_time = time()
calc_time = stop_time - start_time # Total calc time


# Format the digits
if formatting:
	the_digits = format_digits(constant)

else:
	the_digits = constant


if print_or_write == "w":


	print("Writing digits.")

	filename_name = format_const_to_calc() + " - Ramanujan.txt"

	constant_file = open(filename_name, "w")
	constant_file.write(the_digits)
	constant_file.close()
	print("Finished writing digits.")


elif print_or_write == "p":
	newlines()
	print(f"Constant: {format_const_to_calc()}")
	print(f"Algorithm: {fetch_algo(const_to_calc)}\n")


	print(the_digits)

	# Print pretty statistics
	print(f"\nCalculation time: {calc_time:.2f} seconds.")

	if calc_time != 0:
		print(f"Digits per second: {getcontext().prec / calc_time:.2f}")

	stop = input("Press enter to finish the program.")
